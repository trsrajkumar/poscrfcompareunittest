# -*- coding: utf-8 -*-
import unittest
import requests
import json
import xmlrunner
import sys

class InequalityTest(unittest.TestCase):

  urlp2 = ""
    
  def Ontobi_API(self, S1):
        url = 'https://api.ontobi.com/' + self.urlp2 + '/preprocess2'
        payload = {"msgId":"hello","bodyS1":S1}
        headers = {'content-type': 'application/json'}

        session = requests.Session()

        r = session.post(url, data = json.dumps(payload), headers = headers)
        j = r.json()

        output = str(j["data"]["tokenInfo"][0]["dictWordProperty"])

        return output
        
  def test_dictWordUnitTest_1(self):
      text     = "The"
      expected = "DictWord"
      Actuval=self.Ontobi_API(text)
      self.assertEqual(Actuval, expected)
      
  def test_dictWordUnitTest_2(self):
      text     = "run"
      expected = "DictWord"
      Actuval=self.Ontobi_API(text)
      self.assertEqual(Actuval, expected)
      
  def test_dictWordUnitTest_3(self):
      text     = "invoice"
      expected = "DictWord"
      Actuval=self.Ontobi_API(text)
      self.assertEqual(Actuval, expected)
      
if __name__ == '__main__':
    InequalityTest.urlp2 = sys.argv[1]
    suite = unittest.TestLoader().loadTestsFromTestCase(InequalityTest)
    xmlrunner.XMLTestRunner(output='test-reports3').run(suite)